﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Pattern
{
    public class TV
    {
        public void On()
        {
            Console.WriteLine("On TV");
        }
        public void Off()
        {
            Console.WriteLine("Off TV");
        }
    }
}
