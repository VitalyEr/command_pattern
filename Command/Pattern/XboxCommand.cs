﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Pattern
{
    class XboxCommand : ICommand
    {
        Xbox Xbox;
        int time;
        public XboxCommand(Xbox xbox, int t)
        {
            this.Xbox = xbox;
            time = t;
        }

        public void Execute()
        {
            Xbox.On(time);
            Xbox.Off();
        }

        public void Undo()
        {
            Xbox.Off();
        }
    }
}
