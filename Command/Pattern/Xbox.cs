﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Pattern
{
    public class Xbox
    {
        public void On(int time)
        {
            Console.WriteLine("Xbox On during " + time + " min");
        }
        public void Off()
        {
            Console.WriteLine("Xbox Off");
        }
    }
}
