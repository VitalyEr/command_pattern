﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Pattern
{
    public class Pult
    {
        ICommand command;

        public void SetCommand(ICommand command)
        {
            this.command = command;
        }
        public void ButtonPress()
        {
            if(command != null)
            command.Execute();
        }
        public void ButtonUndo()
        {
            if (command != null)
                command.Undo();
        }
    }
}
