﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Pattern
{
    class TvCommand : ICommand
    {
        TV Tv;
        public TvCommand(TV tv)
        {
            this.Tv = tv;
        }
        public void Execute()
        {
            Tv.On();
        }

        public void Undo()
        {
            Tv.Off();
        }
    }
}
