﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Command.Pattern;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            TV tv = new TV();
            TvCommand tvCommand = new TvCommand(tv);
            Xbox xbox = new Xbox();
            XboxCommand xboxCommand = new XboxCommand(xbox, 100);
            Pult pult = new Pult();
            pult.SetCommand(tvCommand);
            pult.ButtonPress();
            pult.SetCommand(xboxCommand);
            pult.ButtonPress();
            Console.WriteLine(new string('-', 10));
            VolumeCommand volumeCommand = new VolumeCommand(new Volume());
            MultiPult multiPult = new MultiPult(3);
            multiPult.SetCommand(0, tvCommand);
            multiPult.PressButton(0);
            multiPult.SetCommand(1, xboxCommand);
            multiPult.PressButton(1);
            multiPult.SetCommand(2, volumeCommand);
            multiPult.PressButton(2);
            multiPult.PressButton(2);
            multiPult.PressButton(2);
            multiPult.PressUndoButton();
            multiPult.PressUndoButton();
            Console.ReadKey();
        }
    }
}
